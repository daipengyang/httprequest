import Alamofire

// MARK: - Request Descriptor -
/**
 Abstract protocol that cannot be directly conforms. Refer to:
 - DataRequestDescriptor
 - DownloadRequestDescriptor
 - UploadRequestDescriptor
 - MultipartUploadRequestDescriptor
 */
public protocol RequestDescriptor {
    associatedtype Request
    var url: URLConvertible { get }
    var method: Alamofire.HTTPMethod { get }
    var headers: Alamofire.HTTPHeaders? { get }
}

public extension RequestDescriptor {
    var headers: Alamofire.HTTPHeaders? { return nil }
}

public protocol DataRequestDescriptor: RequestDescriptor where Request == Alamofire.DataRequest {
    var parameters: Alamofire.Parameters? { get }
    var encoding: Alamofire.ParameterEncoding { get }
}

public protocol DownloadRequestDescriptor: RequestDescriptor where Request == Alamofire.DownloadRequest {
    var parameters: Alamofire.Parameters? { get }
    var encoding: Alamofire.ParameterEncoding { get }
    var destination: DownloadRequest.DownloadFileDestination { get }
}

public extension DownloadRequestDescriptor {
    var parameters: Alamofire.Parameters? { return nil }
}

public extension UploadRequest {
    enum Source {
        case url(URL), data(Data)
    }
    
    typealias NamedSource = (name: String, source: Source)
}

public protocol UploadRequestDescriptor: RequestDescriptor where Request == Alamofire.UploadRequest {
    var source: Request.Source { get }
}

public protocol MultipartUploadRequestDescriptor: RequestDescriptor where Request == Alamofire.UploadRequest {
    var sources: [Request.NamedSource] { get }
}
