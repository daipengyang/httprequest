import Alamofire

// MARK: - Caller -
public typealias RequestCaller = DataRequestCaller & DownloadRequestCaller & UploadRequestCaller & MultipartUploadRequestCaller

/// As RequestDescriptor ≠ Data & Download & Upload RequestDescriptor, Alamofire.SessionManager cannot conforms to RequestDescriptor
//protocol RequestCaller {
//    func request<T>(_ descriptor: T) -> T.Request where T: RequestDescriptor
//}

public protocol DataRequestCaller {
    @discardableResult
    func request<T>(_ descriptor: T) -> T.Request where T: DataRequestDescriptor
}

public protocol DownloadRequestCaller {
    @discardableResult
    func request<T>(_ descriptor: T) -> T.Request where T: DownloadRequestDescriptor
}

public protocol UploadRequestCaller {
    @discardableResult
    func request<T>(_ descriptor: T) -> T.Request where T: UploadRequestDescriptor
}

public protocol MultipartUploadRequestCaller {
    func request<T>(_ descriptor: T, completion: ((Alamofire.Result<T.Request>) -> Void)?) where T: MultipartUploadRequestDescriptor
}

// MARK: Implementation
extension Alamofire.SessionManager: RequestCaller {
    public func request<T>(_ descriptor: T) -> T.Request where T: DataRequestDescriptor {
        return request(descriptor.url,
                       method: descriptor.method,
                       parameters: descriptor.parameters,
                       encoding: descriptor.encoding,
                       headers: descriptor.headers)
    }
    
    public func request<T>(_ descriptor: T) -> T.Request where T: DownloadRequestDescriptor {
        return download(descriptor.url,
                        method: descriptor.method,
                        parameters: descriptor.parameters,
                        encoding: descriptor.encoding,
                        headers: descriptor.headers,
                        to: descriptor.destination)
    }
    
    public func request<T>(_ descriptor: T) -> T.Request where T: UploadRequestDescriptor {
        switch descriptor.source {
        case let .data(data):
            return upload(data, to: descriptor.url)
        case let .url(url):
            return upload(url, to: descriptor.url)
        }
    }
    
    public func request<T>(_ descriptor: T, completion: ((Alamofire.Result<T.Request>) -> Void)? = nil) where T: MultipartUploadRequestDescriptor {
        func appendBodyParts(multipart: Alamofire.MultipartFormData) {
            descriptor.sources.forEach {
                switch $0.source {
                case let .data(data):
                    multipart.append(data, withName: $0.name)
                case let .url(url):
                    multipart.append(url, withName: $0.name)
                }
            }
        }
        
        func subtractRequest(from result: Alamofire.SessionManager.MultipartFormDataEncodingResult) {
            switch result {
            case let .success(request, _, _):
                completion?(.success(request))
            case let .failure(error):
                completion?(.failure(error))
            }
        }
        
        upload(
            multipartFormData: appendBodyParts,
            to: descriptor.url,
            method: descriptor.method,
            headers: descriptor.headers,
            encodingCompletion: subtractRequest)
    }
}
