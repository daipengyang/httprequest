Pod::Spec.new do |s|
  s.name             = 'HTTPRequest'
  s.version          = '1.0.0'
  s.summary          = 'Alamofire-based HTTP Request model protocol.'
  s.description      = <<-DESC
Alamofire provides HTTP standard behavior but lacks request model protocol.
A request model protocol helps encapsulate Alamofire request information.

This pod provides:
* RequestDescriptor for model to describe request
* RequestCaller for HTTP Request caller, like Alamofire, to make HTTP request from a RequestDescriptor
                       DESC
  s.homepage         = 'https://gitlab.com/daipengyang/HTTPRequest'
  s.license          = 'MIT'
  s.author           = { 'DaiPengYang' => 'DaiPengYang.cs99@g2.nctu.edu.tw' }
  s.source           = { :git => 'https://gitlab.com/daipengyang/HTTPRequest.git', :tag => s.version.to_s }
  
  s.source_files = 'Source/*.swift'
  s.dependency 'Alamofire', '~> 4.0'

  # Follow Alamofire
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.10'
  s.tvos.deployment_target = '9.0'
  s.watchos.deployment_target = '2.0'

end
