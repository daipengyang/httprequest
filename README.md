Alamofire provides HTTP standard behavior but lacks request model protocol. This is a request model protocol helps encapsulate Alamofire request information.

This pod provides:
* `RequestDescriptor` for model to describe request
* `RequestCaller` for HTTP Request caller, like Alamofire, to make HTTP request from a `RequestDescriptor`